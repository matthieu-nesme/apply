*[original code](https://maverick.inria.fr/Members/Gilles.Debunne/Code/Apply/) from Gilles Debunne*

# Apply

Apply a command to several files

Apply is a perl script designed for Unix-Linux command-line addicted.

Have you ever dreamed of typing something like (without using xargs) :
```
mv *.c++ *.cpp   
```
Well, simply type :
```
Apply mv #.c++ #.cpp
```
(# replaces the *, which would otherwise be interpreted by your shell before Apply uses it).

The general syntax is :
```
Apply [options] command [to files]
```

Apply is much more powerful than this simple exemple ([`mmv`](http://manpages.ubuntu.com/manpages/bionic/man1/mmv.1.html) is powerful enough to simply move several files). Try `Apply -help` for more details.

-----------------

(don't forget to `chmod 755 Apply`). 